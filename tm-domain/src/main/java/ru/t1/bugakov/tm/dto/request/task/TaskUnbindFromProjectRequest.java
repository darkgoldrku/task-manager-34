package ru.t1.bugakov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUnbindFromProjectRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private String projectId;

    public TaskUnbindFromProjectRequest(@Nullable String taskId, @Nullable String projectId) {
        this.taskId = taskId;
        this.projectId = projectId;
    }

    public TaskUnbindFromProjectRequest(@Nullable String token, @Nullable String taskId, @Nullable String projectId) {
        super(token);
        this.taskId = taskId;
        this.projectId = projectId;
    }

}
