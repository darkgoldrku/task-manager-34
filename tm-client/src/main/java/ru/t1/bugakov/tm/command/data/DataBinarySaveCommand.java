package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataBinarySaveRequest;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE BINARY]");
        getDomainEndpoint().BinarySave(new DataBinarySaveRequest(getToken()));
    }

    @Override
    public @NotNull
    String getName() {
        return "data-save-bin";
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Save data to binary file";
    }

}
