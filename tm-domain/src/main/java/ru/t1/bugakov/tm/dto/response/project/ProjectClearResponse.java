package ru.t1.bugakov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import ru.t1.bugakov.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public final class ProjectClearResponse extends AbstractResponse {
}
