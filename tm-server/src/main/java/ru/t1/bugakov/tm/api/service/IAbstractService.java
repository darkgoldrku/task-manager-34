package ru.t1.bugakov.tm.api.service;

import ru.t1.bugakov.tm.api.repository.IAbstractRepository;
import ru.t1.bugakov.tm.model.AbstractModel;

public interface IAbstractService<M extends AbstractModel> extends IAbstractRepository<M> {

}
