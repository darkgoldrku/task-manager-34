package ru.t1.bugakov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    public void save() {
        bootstrap.getDomainService().dataBackupSave();
    }

    public void load() {
        if (!Files.exists(Paths.get(DomainService.FILE_BACKUP))) return;
        bootstrap.getDomainService().dataBackupLoad();
    }

}
