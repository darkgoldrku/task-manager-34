package ru.t1.bugakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.project.ProjectListRequest;
import ru.t1.bugakov.tm.enumerated.ProjectSort;
import ru.t1.bugakov.tm.enumerated.TaskSort;
import ru.t1.bugakov.tm.model.Project;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        @NotNull List<Project> projects;
        if (sort != null) {
            projects = getProjectEndpoint().listProjects(new ProjectListRequest(getToken(), sort)).getProjects();
        } else {
            projects = getProjectEndpoint().listProjects(new ProjectListRequest(getToken(), null)).getProjects();
        }
        int index = 1;
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show list projects.";
    }

}
