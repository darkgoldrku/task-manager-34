package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataYamlSaveFasterXmlRequest;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE YAML]");
        getDomainEndpoint().YamlSaveFasterXml(new DataYamlSaveFasterXmlRequest(getToken()));
    }

    @Override
    public @NotNull
    String getName() {
        return "data-save-yaml-fasterxml";
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Save data to yaml file with fasterxml";
    }

}
