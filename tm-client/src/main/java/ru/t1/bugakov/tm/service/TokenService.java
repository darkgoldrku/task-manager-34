package ru.t1.bugakov.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.service.ITokenService;

@Setter
@Getter
public final class TokenService implements ITokenService {

    @NotNull
    private String token;

}
