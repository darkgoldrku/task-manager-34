package ru.t1.bugakov.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

public interface CryptUtil {

    @NotNull
    String TYPE = "AES/ECB/PKCS5Padding";

    @NotNull
    @SneakyThrows
    static SecretKeySpec getKey(@NotNull final String secretKey) {
        @NotNull final MessageDigest sha = MessageDigest.getInstance("SHA-1");
        final byte[] key = secretKey.getBytes(StandardCharsets.UTF_8);
        final byte[] digest = sha.digest(key);
        final byte[] secret = Arrays.copyOf(digest, 16);
        return new SecretKeySpec(secret, "AES");
    }

    @NotNull
    @SneakyThrows
    static String encrypt(@NotNull final String secret, @NotNull final String strToEncrypt) {
        @NotNull final SecretKeySpec secretKey = getKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance(TYPE);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        final byte[] bytes = strToEncrypt.getBytes(StandardCharsets.UTF_8);
        return Base64.getEncoder().encodeToString(cipher.doFinal(bytes));
    }

    @NotNull
    @SneakyThrows
    static String decrypt(@NotNull final String secret, @NotNull final String strToDecrypt) {
        getKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance(TYPE);
        cipher.init(Cipher.DECRYPT_MODE, getKey(secret));
        return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
    }

}
