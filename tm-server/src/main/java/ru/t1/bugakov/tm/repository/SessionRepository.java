package ru.t1.bugakov.tm.repository;

import ru.t1.bugakov.tm.api.repository.ISessionRepository;
import ru.t1.bugakov.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}