package ru.t1.bugakov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.Session;
import ru.t1.bugakov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email);

    @NotNull String login(@Nullable String login, @Nullable String password);

    @NotNull
    @SneakyThrows
    Session validateToken(@Nullable String token);

    void invalidate(@Nullable Session session);

}
