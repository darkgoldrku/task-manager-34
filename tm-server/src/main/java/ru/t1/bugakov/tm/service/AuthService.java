package ru.t1.bugakov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.service.IAuthService;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.api.service.ISessionService;
import ru.t1.bugakov.tm.api.service.IUserService;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.exception.field.LoginEmptyException;
import ru.t1.bugakov.tm.exception.field.PasswordEmptyException;
import ru.t1.bugakov.tm.exception.user.AccessDeniedException;
import ru.t1.bugakov.tm.exception.user.PermissionException;
import ru.t1.bugakov.tm.model.Session;
import ru.t1.bugakov.tm.model.User;
import ru.t1.bugakov.tm.util.CryptUtil;
import ru.t1.bugakov.tm.util.HashUtil;

import java.util.Date;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    public AuthService(@NotNull final IUserService userService, @NotNull IPropertyService propertyService, @NotNull ISessionService sessionService) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = userService.findByLogin(login);
        if (user.isLocked()) throw new PermissionException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new PasswordEmptyException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return getToken(user);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final User user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final Session session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private Session createSession(@NotNull final User user) {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return sessionService.add(session);
    }

    @Override
    @NotNull
    @SneakyThrows
    public Session validateToken(@Nullable final String token) {
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = objectMapper.readValue(json, Session.class);

        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        return session;
    }

    @Override
    public void invalidate(@Nullable final Session session) {
        if (session == null) return;
        sessionService.remove(session);
    }

}
