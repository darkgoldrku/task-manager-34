package ru.t1.bugakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.project.ProjectChangeStatusByIndexRequest;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectEndpoint().changeProjectStatusByIndex(new ProjectChangeStatusByIndexRequest(getToken(), index, Status.IN_PROGRESS));
    }

    @NotNull
    @Override
    public String getName() {
        return "project-start-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Start project by index.";
    }

}
