package ru.t1.bugakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.user.UserProfileRequest;
import ru.t1.bugakov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        @Nullable final User user = getAuthEndpoint().profile(new UserProfileRequest(getToken())).getUser();
        if (user != null) {
            System.out.println("[USER PROFILE]");
            System.out.println("ID:" + user.getId());
            System.out.println("LOGIN:" + user.getLogin());
            System.out.println("FIRST NAME:" + user.getFirstName());
            System.out.println("MIDDLE NAME:" + user.getMiddleName());
            System.out.println("LAST NAME:" + user.getLastName());
            System.out.println("E-MAIL:" + user.getEmail());
            System.out.println("ROLE:" + user.getRole());
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "user-view-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "View profile of current user.";
    }

}
