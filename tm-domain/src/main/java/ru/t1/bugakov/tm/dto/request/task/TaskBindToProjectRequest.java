package ru.t1.bugakov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskBindToProjectRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private String projectId;

    public TaskBindToProjectRequest(@Nullable String taskId, @Nullable String projectId) {
        this.taskId = taskId;
        this.projectId = projectId;
    }

    public TaskBindToProjectRequest(@Nullable String token, @Nullable String taskId, @Nullable String projectId) {
        super(token);
        this.taskId = taskId;
        this.projectId = projectId;
    }

}
