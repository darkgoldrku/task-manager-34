package ru.t1.bugakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.user.UserRegistryRequest;
import ru.t1.bugakov.tm.model.User;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final User user = getUserEndpoint().registryUser(new UserRegistryRequest(getToken(), login, password, email)).getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-registry";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Registry new user.";
    }

}
