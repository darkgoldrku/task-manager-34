package ru.t1.bugakov.tm.dto.response.data;

import lombok.NoArgsConstructor;
import ru.t1.bugakov.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public final class DataYamlLoadFasterXmlResponse extends AbstractResponse {
}
