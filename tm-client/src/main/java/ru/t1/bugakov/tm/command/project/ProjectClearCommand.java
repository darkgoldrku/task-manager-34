package ru.t1.bugakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.project.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        getProjectEndpoint().clearProjects(new ProjectClearRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

}
