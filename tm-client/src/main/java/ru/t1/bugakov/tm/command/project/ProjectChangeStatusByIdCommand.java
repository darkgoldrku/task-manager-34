package ru.t1.bugakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        getProjectEndpoint().changeProjectStatusById(new ProjectChangeStatusByIdRequest(getToken(), id, status));
    }

    @NotNull
    @Override
    public String getName() {
        return "project-change-status-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change project status by id.";
    }

}
